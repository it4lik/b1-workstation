# Maîtrise de poste - Day 1

L'objectif de ce TP est de mieux vous faire appréhender l'OS dans lequel vous évoluer tous les jours.

**TOUT** doit être fait depuis la ligne de commande sauf contre-indication. 

**TOUT** doit être réalisé sur votre PC, sur votre OS (pas de VMs, sauf contre-indication).

On va explorer :
* les informations liées à votre OS (liste de périphériques, utilisateurs, etc.)
* l'utilisation des périphériques (partitions de disque, port réseau, etc.)
* la sécurité (sujets autour du chiffrement, la signature, etc.)
* le téléchargement de logiciels (gestionnaire de paquets)
* le contrôle à distance (SSH)
* exposition de service réseau local
* la notion de certificats et de confiance (TLS, chiffrement de mails)

## Index 

<!-- vim-markdown-toc GitLab -->

* [I. Self-footprinting](#i-self-footprinting)
    * [1. Host OS](#1-host-os)
    * [2. Devices](#2-devices)
    * [3. Users](#3-users)
    * [4. Processus](#4-processus)
    * [5. Network](#5-network)
* [II. Scripting](#ii-scripting)
* [III. Gestion de softs](#iii-gestion-de-softs)
* [IV. Machine virtuelle](#iv-machine-virtuelle)
    * [0. Configuration de VirtualBox](#0-configuration-de-virtualbox)
    * [1. Installation de la VM](#1-installation-de-la-vm)
    * [2. Installation de l'OS](#2-installation-de-los)
    * [3. Configuration post-install](#3-configuration-post-install)
    * [4. Partage de fichiers](#4-partage-de-fichiers)
        * [Créer le serveur (sur votre PC)](#créer-le-serveur-sur-votre-pc)
        * [Accéder au partage depuis la VM](#accéder-au-partage-depuis-la-vm)

<!-- vim-markdown-toc -->

# I. Self-footprinting

Première étape : prendre connaissance du système. Etapes peut-être triviales mais néanmoins nécessaires.

**Détailler la marche à suivre pour chacune des étapes.**

## 1. Host OS

🌞 Déterminer les principales informations de votre machine 
* nom de la machine
* OS et version
* architecture processeur (32-bit, 64-bit, ARM, etc)
* quantité RAM et modèle de la RAM

## 2. Devices

Travail sur les périphériques branchés à la machine.

🌞 Trouver
* la marque et le modèle de votre processeur
  * identifier le nombre de processeurs, le nombre de coeur
  * si c'est un proc Intel, expliquer le nom du processeur
* la marque et le modèle :
  * de votre touchpad/trackpad
  * de votre carte graphique

🌞 Disque dur
* identifier la marque et le modèle de votre(vos) disque(s) dur(s)
* identifier les différentes partitions de votre/vos disque(s) dur(s)
* déterminer le système de fichier de chaque partition
* expliquer la fonction de chaque partition

## 3. Users                                                                            

🌞 Déterminer la liste des utilisateurs de la machine
* la liste **complète** des utilisateurs de la machine (je vous vois les Windowsiens...)
* déterminer le nom de l'utilisateur qui est full admin sur la machine
  * il existe toujours un utilisateur particulier qui a le droit de tout faire sur la machine
  * pour les Windowsiens : faites des recherches sur `NT-AUTHORITY\SYSTEM` et le concept de SID

## 4. Processus

🌞 Déterminer la liste des processus de la machine
* je vous épargne l'explication de chacune des lignes
  * bien que ça soit plutôt cool de connaître tous les programmes qui tournent sur sa machine
  * ça peut vite devenir indispensable sur un serveur que l'on veut optimiser
* choisissez 5 services système et expliquer leur utilité
  * par "service système" j'entends des processus élémentaires au bon fonctionnement de la machine
  * sans eux, l'OS tel qu'on l'utilise n'existe pas
  * exemple : interface graphique, démon réseau, etc.
* déterminer les processus lancés par l'utilisateur qui est full admin sur la machine

## 5. Network

🌞 Afficher la liste des cartes réseau de votre machine
* expliquer la fonction de chacune d'entre elles
  * exemple : carte WiFi, carte de loopback, etc.

🌞 Lister tous les ports TCP et UDP en utilisation
* déterminer quel programme tourne derrière chacun des ports
* expliquer la fonction de chacun de ces programmes

# II. Scripting

Le scripting est une approche du développement qui consiste à automatiser de petites tâches simples, mais réalisées à intervalles réguliers ou un grand nombre de fois. 

L'objectif de cette partie est de manipuler un langage de script natif à votre OS. Les principaux avantages d'utiliser un langage natif :
* bah... c'est natif ! Pas besoin d'installation, et il est intégré au système
* le langage est mis à jour automatiquement, en même temps que le système
* le langage est rapide, car souvent bien intégré à l'environnement
* il permet d'accéder de façon simples aux ressources de l'OS (périphériques, processus, etc.) et de les manipuler de façon tout aussi aisée

Vous devrez :
* commenter votre script
* mettre un en-tête à votre script
  * l'en-tête c'est juste un ensemble de commentaires qui renseignent sur le script
  * doivent figurer dans l'en-tête :
    * shebang si besoin (GNU/Linux et MacOS)
    * auteur
    * date
    * description **succincte** de ce que fait le script

🌞 Utiliser un langage de scripting natif à votre OS
* trouvez un langage natif par rapport à votre OS
* l'utiliser pour coder un script qui
  * affiche un résumé de l'OS
    * nom machine
    * IP principale
    * OS et version de l'OS
    * date et heure d'allumage
    * détermine si l'OS est à jour
    * Espace RAM utilisé / Espace RAM dispo
    * Espace disque utilisé / Espace disque dispo
  * liste les utilisateurs de la machine
  * calcule et affiche le temps de réponse moyen vers `8.8.8.8`
    * avec des `ping`
    * **NB :** si vous êtes dans les locaux d'YNOV, le ping vers `8.8.8.8` est impossible. Vous pouvez remplacer par `10.33.3.253` (cette IP n'est joignable **QUE** depuis le réseau d'YNOV)

🐙 ajouter des fonctionnalités au script
* calcule et affiche le débit maximum en download et upload vers internet

Exemple d'output : 
```bash
$ ./footprinting.sh 
Get average ping latency to 8.8.8.8 server...
Get average upload speed...
Get average download speed...

----------------------------------------
nowhere.localhost footprinting report :
----------------------------------------
OS : Arch Linux
Linux kernel : 5.5.9-arch1-2
Is OS up-to-date : false
Up since : 2020-04-29 09:03:07

Interface wlp4s0 : 192.168.1.57/24
  - 8.8.8.8 average ping time : 15.623 ms
  - average download speed : 349.59 Mbit/s
  - average upload speed : 217.76 Mbit/s

RAM
  Used : 3.4Gi
  Free : 2.0Gi
Disk
  Used : 130G
  Free : 90G

Users list : root bin daemon mail ftp http nobody dbus systemd-journal-remote systemd-network systemd-resolve systemd-timesync systemd-coredump uuidd it4 avahi colord polkitd rtkit lightdm usbmux git cups nx nm-openvpn docker gluster rpc netdata dnsmasq toto redis test-cesi tor
```


---

🌞 Créer un deuxième script qui permet, en fonction d'arguments qui lui sont passés :
* exécuter une action
  * lock l'écran après X secondes
  * éteindre le PC après X secondes
* exemple d'utilisation :

```bash
# Verrouile le PC après 30 secondes
$ ./tp1_script2.sh lock 30

# Eteins le PC après 75 secondes
$ ./tp1_script2.sh shutdown 75
```

# III. Gestion de softs

Tous les OS modernes sont équipés ou peuvent être équipés d'un gestionnaire de paquets. Par exemple :
* `apt` pour les GNU/Linux issus de Debian
* `dnf` pour les GNU/Linux issus de RedHat
* `brew` pour macOS
* `chocolatey` pour Windows

🌞 Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets
* par rapport au téléchargement en direct sur internet
* penser à l'identité des gens impliqués dans un téléchargement (vous, l'éditeur logiciel, etc.)
* penser à la sécurité globale impliquée lors d'un téléchargement

🌞 Utiliser un gestionnaire de paquet propres à votre OS pour
* lister tous les paquets déjà installés
* déterminer la provenance des paquets (= quel serveur nous délivre les paquets lorsqu'on installe quelque chose)

# IV. Machine virtuelle

**Le but est de créer une machine virtuelle fonctionnelle à laquelle on peut se connecter en SSH.**
* "machine virtuelle" : virtualiser un PC dans votre PC (très pratique pour tester des choses, ou répartir des ressources)
* "SSH" : protocole qui permet de contrôler une machine à distance

Une connexion SSH, c'est quelque chose que l'on fait quotidiennement, peu importe votre métier dans l'informatique.

---

Vous aurez besoin d'un hyperviseur pour cela. Je vous conseille [VirtualBox](https://www.virtualbox.org/), je ne vous apporterai pas ou peu de support si vous en choisissez un autre.

Vous aurez aussi besoin de l'image d'un OS à installer. Nous installerons ici CentOS 7, que vous pourrez [DL par ici](http://miroir.univ-paris13.fr/centos/7.8.2003/isos/x86_64/CentOS-7-x86_64-Minimal-2003.iso) (miroir officiel).

CentOS7 est un OS GNU/Linux alors adaptez les commandes que vous tapez.

## 0. Configuration de VirtualBox

Créer un réseau privé-hôte *(host-only)* qui porte l'adresse `192.168.120.1`. Cochez la case pour activer le serveur DHCP dans ce réseau.

## 1. Installation de la VM

Créer une machine virtuelle dans VirtualBox avec les paramètres par défaut à part :
* deux cartes réseau 
  * une carte NAT
  * une carte host-only

## 2. Installation de l'OS

Démarrer la machine dans VirtualBox et insérer le `.iso` comme un CD.

Réaliser l'installation de CentOS 7. Presque tout par défaut, les choses à configurer sont les suivantes :
* **langue : anglais**
* clavier : français (azerty ou mac)
* partitionnement par défaut (il faut rentrer dans le menu et faire "Done" pour valider le partitionnement par défaut)
* activer les cartes réseau
* une fois l'installation lancée, créer un utilisateur et définir un mot de passe root
  * n'oubliez pas de cocher la case "faire de cet utilisateur un administrateur" quand vous définissez le mot de passe de l'utilisateur

> `root` c'est le super-utilisateur dans MacOS et GNU/Linux.

## 3. Configuration post-install

Allumer la machine, puis se connecter avec root (ou l'utilisateur créé à l'installation).

Vous pouvez lister les cartes réseau avec :
```bash
$ ip a
```

> Les noms des cartes réseau ressembleront à `enp0s3`, `enp0s8`, etc.

Vous pouvez allumer les cartes réseau avec : 
```bash
$ ifup <CARTE RESEAU>

# Par exemple
$ ifup enp0s8
```

Assurez-vous que les deux cartes réseau sont allumées (elles sont allumée si elles ont une IP).

Effectuez une connexion SSH de votre PC, vers la machine virtuelle, en utilisant le terminal de VOTRE PC (donc votre Powershell si vous êtes sur Windows) :
```bash
# Connexion sur la machine 10.10.10.33, en tant que l'utilisateur Bob :
$ ssh bob@10.10.10.33
```

## 4. Partage de fichiers

### Créer le serveur (sur votre PC)

> Rappel : dans le rendu, la marche à suivre exacte et exhaustive pour réaliser les étapes du TP doit être présente.

Créer un partage de fichiers **SUR VOTRE PC** (pas dans la VM) :
* si vous avez Windows, ce sera avec Samba
  * un simple clic-droit > propriétés
* sous GNU/Linux je vous conseille de faire un partage NFS (ou Samba)
* sous MacOS, vous pouvez aussi faire un partage NFS

### Accéder au partage depuis la VM

Accéder au partage de fichiers depuis la machine virtuelle
* le dossier partagé doit être accessible dans la VM
* vous devez donc pouvoir créer, éditer, et supprimer des fichiers depuis la machine virtuelle
* vous devez prouver que tout ceci fonctionne

> N'hésitez pas à me demander de l'aide pour cette partie.

Marche à suivre (pour les gens sous Windows). **A faire DANS la VM :**
```bash
# Installer le paquet qui permet d'utiliser les partage Samba
$ yum install -y cifs-utils

# Créer un dossier où on accédera au partage
$ mkdir /opt/partage

# Monter le partage dans la VM
$ mount -t cifs -o username=<VOTRE_UTILISATEUR>,password=<VOTRE_MOT_DE_PASSE> //<IP_DE_VOTRE_PC>/<NOM_DU_PARTAGE> /opt/partage
# Par exemple, si :
# - l'utilisateur de votre PC s'appelle Bob
# - votre mot de passe est "toto"
# - que le partaget que vous avez créé s'appelle "share"
# - que votre PC a l'IP 192.168.120.1 (comme j'ai demandé dans le TP)
# la commande sera alors :
$ mount -t cifs -o username=Bob,password=toto //192.168.120.1/share /opt/partage
```

> **ATTENTION** le "username=Bob,password=toto" on parle d'un utilisateur qui existe sur VOTRE PC (pas dans la VM).


