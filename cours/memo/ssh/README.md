# SSH

 Génération de clés

```bash
# Génération d'une clé qui utilise l'algorithme SSH, avec une longueur de clé de 4096 bits
$ ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/home/it4/.ssh/id_rsa):
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /tmp/yolo
Your public key has been saved in /tmp/yolo.pub
The key fingerprint is:
SHA256:xSJnt02Q6Woi0f7CTpMyKpxx0Nn7tHU7KonYarBgr74 it4@nowhere.localhost
The key's randomart image is:
+---[RSA 4096]----+
|          .o     |
|         .o.     |
|  . o.. +.+ .    |
| . o...+ +.+     |
|  .  o. S.. .    |
|.+ ...ooo. .     |
|+ B =oB++ . .    |
|.+ = *o*.  o     |
|.E=........ .    |
+----[SHA256]-----+
```

## Explication des lignes

```bash
Enter file in which to save the key
```

Ici vous renseignez le chemin (ou *path* en anglais) où la clé sera stockée.

---

```bash
Enter passphrase (empty for no passphrase):
Enter same passphrase again: 
```

Cette étape permet de protéger votre clé avec un mot de passe. Une fois protégée avec un mot de passe, il sera nécessaire de le taper à chaque utilisation de la clé.  
Cela renforce grandement la sécurité de la clé, et prévient de son utilisation si on vous la vole.

---

```bash
Your identification has been saved in /tmp/yolo
Your public key has been saved in /tmp/yolo.pub
```

Ces lignes vous informent du chemin où a été stockée votre clé privée ("identification") et votre clé publique ("your public key").
