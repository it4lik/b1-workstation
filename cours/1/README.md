# Cours 1 : Processus/Programmes, IP, TCP&UDP

Le but de ce cours est de proposer une intro à quelques notions élémentaires au sein d'un PC :
* programme et processus
* cartes réseau et adresses IP
* ports TCP et ports UDP

Sommaire :

<!-- vim-markdown-toc GitLab -->

* [I. Programmes et processus](#i-programmes-et-processus)
    * [1. Programme](#1-programme)
    * [2. Processus](#2-processus)
        * [Commandes liées](#commandes-liées)
* [II. Intro réseau](#ii-intro-réseau)
    * [1. Carte réseau](#1-carte-réseau)
        * [Commandes liées](#commandes-liées-1)
    * [2. Adresses IP](#2-adresses-ip)
        * [Commandes liées](#commandes-liées-2)
    * [3. Ports TCP/UDP](#3-ports-tcpudp)
        * [A. Concept](#a-concept)
            * [Commandes liées](#commandes-liées-3)
        * [B. Etablissement de connexion](#b-etablissement-de-connexion)

<!-- vim-markdown-toc -->

# I. Programmes et processus

## 1. Programme

**Un programme est un simple fichier texte, stocké sur stockage froid.** 

Plus précisément, ce fichier texte contient du code (une suite d'instructions) qui est voué à être exécuté au sein de votre système d'exploitation.

> Pour les curieux, un `.exe` contient du code Assembleur *(Assembly Language)*.

Par "stockage froid" on entend un espace de stockage qui n'est qu'assez peu utilisé/accédé pendant que le système est allumé. Les disques durs (HDD ou SSD) et les clés USB sont par exemple de stockage froid. Ce sont des espace de stockage à grande capacité, mais plutôt lent.

Note : les mots "applications", "software", "soft", "logiciel", "programme" (ou toute autre variante) désigne la même chose.

## 2. Processus

**Un *processus* est un programme en cours d'exécution.**

"Exécuter un programme" ça veut dire :
* déplacer le programme d'un stockage froid (et donc lent) vers un stockage chaud (et donc rapide) comme la RAM (mémoire vive)
* demander au processeur (CPU) d'exécuter les instructions contenus dans notre programme, qui se trouve désormais en RAM

Une fois que le programme est placé en RAM, les instructions qu'il contient deviennent accessibles à notre processeur, dans le but de les exécuter.

### Commandes liées

**Lister les *processus* :**
```
# Sur GNU/Linux
$ ps -ef

# Sur MacOS
$ ps aux

# Sur Windows
$ Get-Process
```

# II. Intro réseau

## 1. Carte réseau

Une carte réseau est l'élément de votre système qui permet une connexion à un réseau. Sans carte réseau, votre PC resterait seul au monde :'( .

Il existe souvent au moins deux cartes réseau dans nos PCs récents :
* une carte Ethernet
  * elle est physiquement placée derrière la prise Ethernet/RJ45 de votre PC
  * elle permet une connexion filaire
* une carte WiFi
  * elle est munie d'une antenne pour capter et envoyer des signaux WiFi
  * elle permet une connexion sans fil

**Une carte réseau permet d'avoir une adresse IP.**

Sur toutes les machines existe une ***interface de loopback***. Cette interface porte toujours l'IP `127.0.0.1` et permet de se joindre soi-même.

### Commandes liées

Lister les cartes réseau :
```bash
# GNU/Linux
$ ip a
$ ifconfig

# MacOS
$ ifconfig

# Windows
$ ipconfig
```

Envoyer un message simple à une IP donnée (avec la commande `ping`) :
```bash
# Ping de l'interface de loopback = se ping soi-même
$ ping 127.0.0.1

# Ping de quelqu'un sur le même réseau que nous
# Dans l'exemple ci-dessous, 10.33.3.253 est l'adresse d'une machine dans le réseau YNOV
$ ping 10.33.3.253
```

## 2. Adresses IP

Une adresse IP est similaire à une adresse postale en tout point : elle définit votre adresse unique au sein d'un réseau donné. 

Au même titre qu'on peut avoir deux personnes ayant la même adresse postale dans deux villes différentes, il est parfaitement possible d'avoir deux interfaces réseau qui portent la même adresse IP du moment qu'elles ne sont pas connectées au même réseau.

**Une adresse IP est portée par une carte réseau : il FAUT une carte réseau pour avoir une adresse IP.**

### Commandes liées

Identifier une adresse IP (l'exemple est donné sous GNU/Linux, mais c'est similaire sur tous les OS) :
```bash
$ ip a

enp0s31f6: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether c8:5b:76:1b:3c:81  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 16  memory 0xf1200000-f1220000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 284  bytes 19108 (18.6 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 284  bytes 19108 (18.6 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlp4s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.33.1.184  netmask 255.255.252.0  broadcast 10.33.3.255
        inet6 fe80::61be:f02c:289f:9077  prefixlen 64  scopeid 0x20<link>
        ether e4:b3:18:48:36:68  txqueuelen 1000  (Ethernet)
        RX packets 28903  bytes 23554787 (22.4 MiB)
        RX errors 0  dropped 1  overruns 0  frame 0
        TX packets 11375  bytes 1354604 (1.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Explication de la sortie de la commande :
* `enp0s31f6` est ma carte Ethernet
* `lo` est ma carte de *loopback*
* `wlp4s0` est ma carte WiFi
  * elle porte l'adresse IP `10.33.1.184`

## 3. Ports TCP/UDP

### A. Concept

Pour des messages simples, comme un `ping`, on peut contacter une IP en direct.

Pour des communications plus complexes, comme par exemple la visite d'un serveur Web, il est nécessaire d'utiliser la notion de *ports*.

**Un *port* est une porte d'entrée sur une machine donnée.**  
Plus précisément, un *port* est une porte d'entrée sur une interface réseau donnée. Il existe 65535 *ports TCP* et 65535 *ports UDP* sur chaque interface réseau.

Il y a deux façon de se servir d'un *port* :
* soit on est le **serveur** et on décide de quel *port* on veut utiliser spécifiquement
  * l'administrateur va exécuter un processus, et le placer derrière un *port*
  * **on dit alors que le *processus* *"écoute"* sur le *port***
  * une fois lancé, le *processus* en écoute attend des connexions de potentiels clients
* soit on est le **client** et on décide à quel serveur on veut se connecter
  * le client lance une application capable de se connecter à des *ports* distants (comme un navigateur Web pour se connecter à des sites Web)
  * le client utilise l'application pour demander une connexion à une IP et un *port* spécifique

> Note : pour les protocoles les plus utilisés, il existe des standards pour les numéros de *port*. Par exemple, HTTP c'est 80/TCP, HTTPS c'est 443/TCP, SSH c'est 22/TCP, etc. Histoire d'étendre les exemples à des trucs moins techniques : le client Battle.net utilise 80/TCP, 80/UDP, 443/TCP, 443/UDP, 1119/TCP et 1119/UDP, etc.

---

Les deux familles de *ports* :
* TCP : connexion stable, sécurisée, mais lente
  * utilisé pour HTTP, HTTPS, SSH, etc.
* UDP : connexion instable mais rapide
  * utilisé par les torrents, les jeux en ligne, etc.

#### Commandes liées

Visualiser les *ports* utilisés actuellement par le système :
```bash
# GNU/Linux
## Ports client TCP
$ sudo ss -apn -t
## Port client UDP
$ sudo ss -apn -u
## Port serveur TCP
$ sudo ss -anp -t -l

# Windows/MacOS (dans un terminal Administrateur)
## Ports client TCP
$ netstat -b -n -p tcp
## Ports client UDP
$ netstat -b -n -p tcp
## Afficher les ports serveur TCP (écoute) en plus des ports client TCP
$ netstat -b -n -p tcp -a
## Afficher les ports serveur UDP (écoute) en plus des ports client UDP
$ netstat -b -n -p udp -a
```

### B. Etablissement de connexion

La suite d'étape lorsque l'on se connecte à un serveur distant est la suivante :
1. L'administrateur du serveur a lancé un *processus* sur le serveur (par exemple un serveur Web)
2. L'administrateur a configuré le *processus* pour "écouter" derrière un *port*
3. Le client **connaît** l'adresse IP du serveur, ainsi que le *port* où le *processus* distant écoute
4. Le client lance un *processus* qui permet de se connecter à distance (par exemple un navigateur Web)
5. Le *processus* du client ouvre un *port* aléatoire sur la machine du client
6. Le *processus* du client établit une connexion entre le *port* du client, et le *port* distant du serveur
7. La connexion est établie entre le client et le serveur

On peut schématiser la connexion sous la forme : `IP_Client:Port_client <---> IP_Serveur:Port_Serveur`.
